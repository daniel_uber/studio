# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171127002258) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "owner_id"
    t.index ["owner_id"], name: "index_accounts_on_owner_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "street"
    t.string "apt"
    t.string "city"
    t.integer "zip"
    t.bigint "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state", default: "IL"
    t.index ["member_id"], name: "index_addresses_on_member_id"
  end

  create_table "codes", force: :cascade do |t|
    t.date "start"
    t.date "finish"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "next_code", comment: "the next code"
    t.boolean "active", comment: "whether this is the current code"
  end

  create_table "members", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "phones", force: :cascade do |t|
    t.string "phone_number"
    t.bigint "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_phones_on_member_id"
  end

  create_table "rates", force: :cascade do |t|
    t.string "name"
    t.integer "hourly", comment: "the hourly rate"
    t.integer "daily", comment: "daily max rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "second_station", comment: "second station rate"
  end

  create_table "reservations", force: :cascade do |t|
    t.bigint "station_id"
    t.date "day"
    t.time "start"
    t.time "finish"
    t.boolean "daily"
    t.bigint "account_id"
    t.bigint "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_reservations_on_account_id"
    t.index ["member_id"], name: "index_reservations_on_member_id"
    t.index ["station_id"], name: "index_reservations_on_station_id"
  end

  create_table "stations", force: :cascade do |t|
    t.string "name"
    t.bigint "rate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rate_id"], name: "index_stations_on_rate_id"
  end

  add_foreign_key "accounts", "members", column: "owner_id"
  add_foreign_key "addresses", "members"
  add_foreign_key "phones", "members"
  add_foreign_key "reservations", "accounts"
  add_foreign_key "reservations", "members"
  add_foreign_key "reservations", "stations"
  add_foreign_key "stations", "rates"
end
