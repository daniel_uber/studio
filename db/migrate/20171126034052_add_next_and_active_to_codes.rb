class AddNextAndActiveToCodes < ActiveRecord::Migration[5.1]
  def change
    add_column(:codes, :next_code, :bigint, comment: "the next code")
    add_column(:codes, :active, :boolean, comment: "whether this is the current code")
  end
end
