class AlterRatesToUseIntegerMoney < ActiveRecord::Migration[5.1]
  def change
    change_column(:rates, :hourly, :integer, comment: "the hourly rate")
    change_column(:rates, :daily, :integer, comment: "daily max rate")
    add_column(:rates, :second_station, :integer, comment: "second station rate")
  end
end
