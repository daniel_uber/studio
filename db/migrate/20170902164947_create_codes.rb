class CreateCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :codes do |t|
      t.date :start
      t.date :end
      t.integer :value

      t.timestamps
    end
  end
end
