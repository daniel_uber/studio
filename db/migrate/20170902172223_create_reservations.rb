class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.references :station, foreign_key: true
      t.date :day
      t.time :start
      t.time :end
      t.boolean :daily
      t.references :account, foreign_key: true
      t.references :member, foreign_key: true

      t.timestamps
    end
  end
end
