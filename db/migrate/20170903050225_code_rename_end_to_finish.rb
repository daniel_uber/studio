class CodeRenameEndToFinish < ActiveRecord::Migration[5.1]
  def change
    rename_column(:codes, :end, :finish)
  end
end
