class CreateRates < ActiveRecord::Migration[5.1]
  def change
    create_table :rates do |t|
      t.string :name
      t.decimal :hourly
      t.decimal :daily

      t.timestamps
    end
  end
end
