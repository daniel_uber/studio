class ReservationsRenameEndToFinish < ActiveRecord::Migration[5.1]
  def change
    rename_column(:reservations, :end, :finish)
  end
end
