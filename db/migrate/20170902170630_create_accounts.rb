class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.timestamps
    end
    # reference Account#owner as a member
    add_reference :accounts, :owner, references: :members, index: true
    add_foreign_key :accounts, :members, column: :owner_id
  end
end
