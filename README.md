# README
Studio application

[![CircleCI](https://circleci.com/bb/daniel_uber/studio/tree/master.svg?style=svg)](https://circleci.com/bb/daniel_uber/studio/tree/master)

Things you may want to cover:

* Ruby version 2.4+

* Gemfile and database are configured for Passenger and Postgresql, but no known
dependency exists on these

* How to run the test suite
bundle exec rspec (documentation format is on by default, suite is randomized, tailor to suit) 

