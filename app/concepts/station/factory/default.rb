class Station::Default
  def self.static_params
    {
     station: {
               name: "Some Station",
               rate: nil
              }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:station] = p[:station]
    .merge(rate: Rate::Default.())
    .merge(options)
    p
  end

  def self.call
    Station::Create.(params)["model"]
  end
end
