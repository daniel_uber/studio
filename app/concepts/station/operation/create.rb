require_relative '../contract/create'

class Station::Create < Trailblazer::Operation
  step Model(Station, :new)
  step Contract::Build(constant: Station::Contract::Create)
  step Contract::Validate(key: :station)
  step Contract::Persist()
end
