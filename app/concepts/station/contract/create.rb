require 'reform'
require 'reform/form/dry'

module Station::Contract
  class Create < Reform::Form
    include Dry

    property :name
    property :rate

    validation :default do
      required(:name).filled
      required(:rate).filled
    end

    validation :existence, if: :default do
      configure do
        predicates(Verifying)
      end

      required(:rate).is_record?(Rate)
    end
  end
end
