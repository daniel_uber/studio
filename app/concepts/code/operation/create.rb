require_relative '../contract/create'

class Code::Create < Trailblazer::Operation
  step Model( Code, :new)
  step Contract::Build( constant: Code::Contract::Create)
  step Contract::Validate( key: :code)
  step Contract::Persist()
end
