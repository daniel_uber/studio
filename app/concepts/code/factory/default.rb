class Code::Default
  def self.static_params
    {code: {
            value: 1234,
            start: 7.days.ago,
            active: true,
            next: nil,
            finish: 2.days.ago }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:code] = p[:code].merge(options)
    p
  end

  def self.call
    Code::Create.(params)["model"]
  end
end
