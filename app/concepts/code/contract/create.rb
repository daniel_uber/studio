require 'reform'
require 'reform/form/dry'

module Code::Contract
  class Create < Reform::Form
    include Dry

    property :value
    property :start
    property :finish
    property :next_code
    property :active

    validation do
      configure do
        def forward_time?(start, finish)
          start < finish
        end

        def four_uniq_digits?(value)
          digits = value.to_s.split('')
          digits.count == 4 &&  digits.uniq.count == 4
        end

        def active_has_no_next?(active, next_code)
          (!active) || (!next_code)
        end
      end

      required(:value).filled
      required(:value).four_uniq_digits?
      required(:start).filled
      required(:finish).filled
      required(:start).time?
      required(:finish).time?
      required(:start,:finish).forward_time?
      required(:active, :next_code).active_has_no_next?
    end
  end
end
