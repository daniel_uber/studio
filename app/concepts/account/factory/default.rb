class Account::Default
  def self.static_params
    {
     account: {
               owner: nil
              }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:account] = p[:account]
    .merge(owner: Member::Default.() )
    .merge(options)
    p
  end

  def self.call
    Account::Create.(params)["model"]
  end
end
