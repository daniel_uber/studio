require 'reform'
require 'reform/form/dry'

module Account::Contract
  class Create < Reform::Form
    include Dry

    property :owner
    
    validation :default do
      required(:owner).filled
    end

    validation :existence, if: :default do
      configure do
        predicates(Verifying)
      end

      required(:owner).is_record?(Member)
    end
  end
end
