require_relative '../contract/create'

class Account::Create < Trailblazer::Operation
  step Model(Account, :new)
  step Contract::Build( constant: Account::Contract::Create )
  step Contract::Validate(key: :account)
  step Contract::Persist()
end
