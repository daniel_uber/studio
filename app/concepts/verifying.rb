require 'dry/logic'

# to use this module, you probably want "predicates(Verifying)"
# in a validation configure block

module Verifying
  include Dry::Logic::Predicates

  predicate(:is_record?) do |obj, klass|
    klass.where(id: obj.id).presence
  end

  predicate(:is_id?) do |id, klass|
    klass.where(id: id.to_i).presence
  end
  
  predicate(:precedes?) do  |t1, t2|
    t1 < t2
  end
end
