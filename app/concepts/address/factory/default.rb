class Address::Default
  def self.call
    Address::Create.(params)["model"]
  end

  def self.static_params
    {
     address: {
               street: "123 Fake St",
               apt: "2S",
               state: "TX",
               city: "Anytown",
               zip: 12345,
               member: nil
              }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:address] = p[:address]
    .merge(member: Member::Default.() )
    .merge(options)
    p
  end
end
