require 'reform'
require 'reform/form/dry'

module Address::Contract
  class Create < Reform::Form
    include Dry

    property :street
    property :apt
    property :city
    property :state
    property :zip
    property :member

    validation :default do
      required(:street).filled
      required(:city).filled
      required(:zip).filled
    end

    validation :existence, if: :default do
      configure do
        predicates(Verifying)
      end

      required(:member).is_record?(Member)
    end
  end
end
