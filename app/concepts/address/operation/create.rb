require_relative '../contract/create'

class Address::Create < Trailblazer::Operation
  step Model(Address, :new)
  step Contract::Build( constant: Address::Contract::Create )
  step Contract::Validate( key: :address)
  step Contract::Persist()
end
