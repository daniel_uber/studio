require_relative '../contract/create'

class Phone::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step :populate_members
    step Model(Phone, :new)
    step Contract::Build(constant: Phone::Contract::Create)

    def populate_members(options, *)
      options["members"] = Member.all
    end
  end
  
  step Nested(Present)
  step Contract::Validate(key: :phone)
  step Contract::Persist()
end
