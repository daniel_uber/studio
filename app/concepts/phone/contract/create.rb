require 'reform'
require 'reform/form/dry'

module Phone::Contract
  class Create < Reform::Form
    include Dry

    property :phone_number
    property :member_id

    validation :default do
      required(:phone_number).filled
    end

    validation :existence, if: :default do
      configure do
        predicates(Verifying)
      end

      required(:member_id).is_id?(Member)
    end
  end
end
