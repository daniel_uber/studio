class Phone::Default
  def self.call
    Phone::Create.(params)["model"]
  end

  def self.static_params
    {
     phone: {
             phone_number: "202-555-2876",
             member_id: nil
            }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:phone] = p[:phone]
    .merge(member_id: Member::Default.().id)
    .merge(options)
    p
  end
end
