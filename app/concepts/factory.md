# Default Factory Class

I was feeling like unrelated specs needed to know too much about the classes they were creating

The goal here was to make a "default params", and optionally a "default instance".

Each Factory module should be defined as a class Default, with three class methods:

- static_params : return a wrapped hash with no database records (only literals)
- params: return a fully ready set of params with any relations satisfied (may touch db/create objects)
- call : return the default instance, should be the same a calling Create.(params).


This happened after some trial and error where I was mistakenly modifying "constant" nested
hashes during tests and seeing foreign key contraints.
