class Rate::Default
  def self.static_params
    {
     rate: {
            name: "Some Rate",
            hourly: 5.00,
            daily: 35.00,
           }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:rate] = p[:rate].merge(options)
    p
  end

  def self.call
    Rate::Create.(params)["model"]
  end

end
