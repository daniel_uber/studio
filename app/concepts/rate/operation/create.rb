require_relative '../contract/create'

class Rate::Create < Trailblazer::Operation
  step Model(Rate, :new)
  step Contract::Build(constant: Rate::Contract::Create )
  step Contract::Validate(key: :rate)
  step Contract::Persist()
end
