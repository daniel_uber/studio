require 'reform'
require 'reform/form/dry'

module Rate::Contract
  class Create < Reform::Form
    include Dry

    property :hourly
    property :daily
    property :name
    property :second_station

    validation do
      required(:name).filled
      required(:hourly).filled
      required(:daily).filled

      required(:hourly) { int? & gt?(0) }
      required(:daily) { int? & gt?(0) }
      required(:second_station) { int? & gt?(0) | none? }
    end
  end
end
