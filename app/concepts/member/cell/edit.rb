module Member::Cell
  class Edit < New
    property :firstname
    property :lastname
    property :email
    
    def show
      render :edit
    end
    
    def delete
      link_to "Delete", member_path(model.id), method: :delete, data: {confirm: 'Are you sure?'}
    end

    def back
      link_to "Back to members list", member_path
    end
  end
end


