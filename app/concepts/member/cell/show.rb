module Member::Cell
  class Show < Trailblazer::Cell
    property :firstname
    property :lastname
    property :email
    
    def edit
      link_to "Edit", edit_member_path(model.id)
    end

    def delete
      link_to "Delete", member_path(model.id), method: :delete, data: {confirm: 'Are you sure?'}
    end

    def back
      link_to "Back to members list", member_path
    end
  end
end


