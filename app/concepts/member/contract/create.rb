require 'reform'
require 'reform/form/dry'

module Member::Contract
  class Create < Reform::Form
    include Dry

    property :firstname
    property :lastname
    property :email

    validation do
      required(:firstname).filled
      required(:lastname).filled
      required(:email).filled
    end
  end
end
