class Member::Show < Trailblazer::Operation
  step Model(Member, :find_by)
end
