require_relative '../contract/update'

class Member::Update < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Member, :find_by)
    step Contract::Build( constant: Member::Contract::Update )
  end
  step Nested( Present)
  step Contract::Validate( key: :member)
  step Contract::Persist()
end
