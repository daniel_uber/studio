class Member::Index < Trailblazer::Operation
  step :model!

  def model!(options, *)
    options["model"] = ::Member.all
  end
end
