require_relative '../contract/create'

class Member::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Member, :new)
    step Contract::Build( constant: Member::Contract::Create )
  end

  step Nested(Present)
  step Contract::Validate( key: :member)
  step Contract::Persist()
end

