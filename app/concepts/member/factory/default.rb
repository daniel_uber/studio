class Member::Default
  def self.static_params
    {
     member: {
              firstname: "Foo",
              lastname: "Bar",
              email: "foo@bar.baz"
             }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:member] = p[:member].merge(options)
    p
  end

  def self.call
    Member::Create.(params)["model"]
  end
end
