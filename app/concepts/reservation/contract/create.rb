require 'reform'
require 'reform/form/dry'

module Reservation::Contract
  class Create < Reform::Form
    include Dry

    property :start
    property :finish
    property :account
    property :day
    property :station
    property :member

    validation :default do
      required(:start).time?
      required(:start).filled
      required(:finish).filled
      required(:finish).time?
      required(:day).filled
      required(:day).date?
      required(:station).filled
      required(:account).filled
      required(:member).filled
    end

    validation :existence, if: :default do
      configure do
        predicates(Verifying)
      end

      required(:account).is_record?(Account)
      required(:station).is_record?(Station)
      required(:member).is_record?(Member)
      required(:start).precedes?( :finish)
    end
  end
end
