class Reservation::Default
  def self.static_params
    today = Date.today
    {
      reservation: {
                    day: today,
                    start: today.midnight + 8.hours,
                    finish: today.midnight + 10.hours,
                    station: nil,
                    member: nil,
                    account: nil
                   }
    }
  end

  def self.params(options = {})
    p = static_params
    p[:reservation] = p[:reservation]
    .merge(
           member: Member::Default.(),
           account: Account::Default.(),
           station: Station::Default.()
          )
    .merge(options)
    p
  end

  def self.call
    Reservation::Create.(params)["model"]
  end
end
