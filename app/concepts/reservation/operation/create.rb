require_relative '../contract/create'

class Reservation::Create < Trailblazer::Operation
  step Model(Reservation, :new)
  step Contract::Build(constant: Reservation::Contract::Create)
  step Contract::Validate(key: :reservation)
  step Contract::Persist()
end
