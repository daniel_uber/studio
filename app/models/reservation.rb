class Reservation < ApplicationRecord
  belongs_to :station
  belongs_to :account
  belongs_to :member
end
