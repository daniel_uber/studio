class Account < ApplicationRecord
  belongs_to :owner, class_name: "Member"
  has_many :members
end
