class Member < ApplicationRecord
  has_one :account, foreign_key: "owner_id"
  has_many :phones
  has_many :addresses

  def to_s
    "#{firstname} #{lastname} <#{email}> (#{id})"
  end
end
