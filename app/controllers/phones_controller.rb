class PhonesController < ApplicationController
  def new
    run Phone::Create::Present
    render cell(Phone::Cell::New, @form)
  end

  def create
    run Phone::Create
  end
end
