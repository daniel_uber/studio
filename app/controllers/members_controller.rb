class MembersController < ApplicationController
  def index
      run Member::Index
      render cell(Member::Cell::Index, result["model"])
  end

  def show
    run Member::Show
    render cell(Member::Cell::Show, result["model"])
  end
  
  def new
    run Member::Create::Present
    render cell(Member::Cell::New, @form)
  end
  
  def edit
    run Member::Update::Present
    render cell(Member::Cell::Edit, @form)
  end

  def create
    run Member::Create do |result|
      return redirect_to members_path
    end
    
    render cell(Member::Cell::New, @form)
  end
  
  def update
    run Member::Update do |result|
      return redirect_to member_path(result["model"].id)
    end
  
    render cell(Member::Cell::Edit, @form)
  end
end
