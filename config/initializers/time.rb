class Studio::Application

  config.time_zone = 'Central Time (US & Canada)'
  config.active_record.default_timezone = :local
end
