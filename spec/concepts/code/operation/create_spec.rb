require 'rails_helper'

RSpec.describe Code::Create do
  let(:params) { {code:
                  {value: 1234,
                   start: 7.days.ago,
                   finish: 2.days.ago } }}

  context 'with valid params' do
    it "creates a code" do
      result = Code::Create.(params)
      expect(result.success?).to be true
      expect(result["model"].class).to be Code
    end
  end

  it "fails when no value" do
    params[:code].delete(:value)
    result = Code::Create.(params)
    expect(result.failure?).to be true
  end

  it "fails when no start" do
    params[:code].delete(:start)
    result = Code::Create.(params)
    expect(result.failure?).to be true
  end
end
