require 'rails_helper'

RSpec.describe Rate::Create do
  let(:params) { Rate::Default.params }

  context 'with valid params' do
    it "creates a rate" do
      result = Rate::Create.(params)
      expect(result.success?).to be true
      expect(result["model"].class).to be Rate
    end
  end

  it "fails when no rates" do
    params[:rate].delete(:hourly)
    result = Rate::Create.(params)
    expect(result.failure?).to be true
  end
end
