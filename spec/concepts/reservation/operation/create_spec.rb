require 'rails_helper'

RSpec.describe Reservation::Create do
  let (:params) { Reservation::Default.params }

  it "with valid params" do
    result = Reservation::Create.(params)
    expect(result.success?).to be true
    expect(result["model"].class).to eq Reservation
  end
end
