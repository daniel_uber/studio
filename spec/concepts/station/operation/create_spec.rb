require 'rails_helper'

RSpec.describe Station::Create do
  let(:params) { Station::Default.params }

  context 'with valid params' do
    it "creates a station" do
      result = Station::Create.(params)
      expect(result.success?).to be true
      expect(result["model"].class).to be Station
    end
  end

  it "fails when no name" do
    params[:station].delete(:name)
    result = Station::Create.(params)
    expect(result.failure?).to be true
  end

  it "fails when no rate" do
    result = Station::Create.(Station::Default.static_params)
    expect(result.failure?).to be true
  end
end
