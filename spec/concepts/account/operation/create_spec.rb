require 'rails_helper'

RSpec.describe Account::Create do
  it "accepts an owner" do
    default_email = Member::Default.static_params[:member][:email]
    result = Account::Create.(Account::Default.params)

    expect(result.success?).to be true
    model = result["model"]
    expect(model.owner.email).to eq default_email
  end

  it "fails when no owner" do
    result = Account::Create.({no_owner: 2, junk: true})
    expect(result.failure?).to be true
  end
end
