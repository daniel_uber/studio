require 'rails_helper'

RSpec.describe Address::Create do
  it "accepts valid params" do
    result = Address::Create.(Address::Default.params)
    model = result["model"]
    expect(result.success?).to be true
    expect(model.state).to eq("TX")
  end

  it "requires valid member" do
    result = Address::Create.(Address::Default.static_params)
    expect(result.success?).to be false
  end
end
