require 'rails_helper'

RSpec.describe Phone::Create do
  it "accepts valid params" do
    result = Phone::Create.(Phone::Default.params)
    expect(result.success?).to be true
  end

  it "requires a phone number" do
    result = Phone::Create.({phone: {nonsense: :keys}})
    expect(result.failure?).to be true
  end
end
