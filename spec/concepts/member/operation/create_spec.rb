require 'rails_helper'

RSpec.describe Member::Create do
  let(:params) { {member: {firstname: "Test", lastname: "User", email: "test@user.domain"}}}

  context 'with valid params' do
    it "creates a member" do
      result = Member::Create.(params)

      expect(result.success?).to be true
      expect(result["model"].class).to be Member
      expect(result["model"].lastname).to eq "User"
    end
  end

  it "fails when no email" do
    params[:member].delete(:email)

    result = Member::Create.(params)

    expect(result.failure?).to be true
  end

  it "fails when no names" do
    params[:member].delete(:firstname)

    result = Member::Create.(params)

    expect(result.failure?).to be true
  end
end
